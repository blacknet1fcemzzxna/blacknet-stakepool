const readline = require('readline');
const getaddress = require('./lib/account/getaddress')
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
rl.stdoutMuted = true;

question();

global.poolOwner = '';
global.mnemonic = '';
rl._writeToOutput = function _writeToOutput(stringToWrite) {
    if (rl.stdoutMuted)
        rl.output.write("");
    else
        rl.output.write(stringToWrite);
};


function question(){

    rl.question('Please input your Pool Mnemonic Code: ', (answer) => {

        let kp = getaddress.blacknet_mnemonic_keypair(answer);

        if(!kp){
            console.log('Wrong Mnemonic Code, Input again: ')
            question();
        }else{
            let account = getaddress.blacknet_pk_account(kp.publicKey);
            console.log(`Your PoS Pool account is ${account}.`);
            mnemonic = answer;
            poolOwner = account;
            
            require('./tools/payment');
        }
    });
}