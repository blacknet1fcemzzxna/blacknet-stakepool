


const Router = require('koa-router');
const router = new Router({
    prefix: '/api'
});

const API = require('../lib/api');


router.get('/website_data', async (ctx, next) => {

    let workers = await Worker.find({ poolOwner }).sort({ hashrate: 'desc' });

    let data = {};
    let hashrate = 0, number = 0;
    for (let worker of workers) {

        hashrate += worker.hashrate;
        if (worker.hashrate > 1e8) number++;
    }

    let block = await Transaction.findOne({ type: 254, poolOwner }).sort({ "data.blockHeight": -1 });

    data.hashrate = hashrate / 1e8;
    data.number = number;
    data.blockHeight = block.blockHeight;
    data.blockHash = block.blockHash;

    data.time = block.time + '000';
    data.poolOwner = poolOwner;



    ctx.body = data;
});

router.get('/workers', async (ctx, next) => {

    let workers = await Worker.find({ poolOwner }).sort({ hashrate: 'desc' }).lean();
    for (let w of workers) {

        let status = await getPayoutStatus(w.address);
        w.paid = status.paid;
        w.total_reward = status.total_reward;
        w.payouts = status.payouts;
        w.hashrate = parseInt(w.hashrate / 1e8)
    }
    ctx.body = workers;
});


router.get('/worker/:address', async (ctx, next) => {
    let address = ctx.params.address;

    let worker = await Worker.findOne({ poolOwner, address }).lean();

    if(!worker) return ctx.body = {
        paid: 0,
        total_reward: 0,
        payouts: 0,
        hashrate: 0
    };

    let status = await getPayoutStatus(worker.address);
    worker.paid = parseFloat(status.paid);
    worker.total_reward = parseFloat(status.total_reward);
    worker.payouts = status.payouts;
    worker.hashrate = parseInt(worker.hashrate / 1e8)
    ctx.body = worker;
});

router.get('/recent_blocks', async (ctx, next) => {

    let nodeinfo = await API.getInfo();
    if(!nodeinfo) nodeinfo = {};
    let txns = await Transaction.find({ type: 254, poolOwner }).limit(100).sort({ "data.blockHeight": -1 });
    ctx.body = {
        txns,
        nodeinfo
    };
});

router.get('/recent_payouts', async (ctx, next) => {
    ctx.body = await Payout.find({ isPayout: true, poolOwner }).limit(100).sort({ blockHeight: 'desc' });
});

router.get('/payouts/:blockHeight', async (ctx, next) => {
    let blockHeight = ctx.params.blockHeight;
    let list = await Payout.find({ blockHeight, poolOwner, shares: { $gt: 0 } });

    list = list.map((i) => {
        i.shares = parseInt(i.shares / 1e8);
        return i;
    })
    ctx.body = list;
});

router.get('/account/:account', async (ctx, next) => {

    let payments = [], page = ctx.query.page || 1;
    let account = ctx.params.account.toLowerCase(), query = { account, poolOwner };

    payments = await Payout.find(query).skip((page - 1) * 500).limit(500).sort({ blockHeight: 'desc' }).lean();

    ctx.body = payments;
});



module.exports = router;




async function getPayoutStatus(account) {

    let paid = 0, total_reward = 0;
    let t = await Payout.aggregate(
        [{ $match: { account } },
        {
            $group: {
                _id: null,
                total: { $sum: "$amount" }
            }
        }]
    );

    let p = await Payout.aggregate(
        [{ $match: { isPayout: true } },{ $match: { account} },
        {
            $group: {
                _id: null,
                total: { $sum: "$amount" }
            }
        }]
    );
    if (p[0] && t[0]) {

        paid = (p[0].total / 1e8).toFixed(8);
        total_reward = (t[0].total / 1e8).toFixed(8);
    }

    let payouts = await Payout.countDocuments({ account: account });

    return { paid, total_reward, payouts }
}
