const request = require("request");
const rp = require('request-promise');
const headers = {
    'User-Agent': 'Mozilla/5.0 (Linux; Android 8.0; Pixel 2 Build/OPD3.170816.012) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Mobile Safari/537.36'
};
const hosts = [
    config.api
];
let host_index = 0;

module.exports = {

    getInfo: () => {
        return getApi('/ledger');
    },

    getNodeInfo: () => {
        return getApi('/nodeinfo');
    },

    getPeerDB: () => {
        return getApi('/peerdb');
    },

    getTxPool: () => {
        return getApi('/txpool');
    },

    getTxWithTxHash: (tx) => {
        return getApi('/walletdb/gettransaction/' + tx + '/false');
    },

    getPeerInfo: () => {
        return getApi('/peerinfo');
    },

    getBlockHash: (height) => {

        return getApi(`/blockdb/getblockhash/${height}`);
    },

    getBlock: (hash, withTransactionOrNot) => {

        return getApi(`/blockdb/get/${hash}/${withTransactionOrNot}`);
    },

    getBalance: (address) => {

        return getApi(`/ledger/get/${address}`);
    },

    verifySign: (account, signature, message) => {
        return getApi("/verifymessage/" + account + "/" + signature + "/" + message + "/");
    },
    sendMoney
}

function getHost(){

    if(hosts[host_index]) return hosts[host_index];

    host_index = 0;

    return hosts[0];
}


function getApi(url) {
    
    return fetch(url);
}


async function fetch(url){
    
    let data, i = hosts.length;
    
    while(i-->0){
        data = await process(url);
        if(data === null) break;

        if(data.error == 'ECONNREFUSED'){
            host_index++;
            continue;
        }
    }
    return data;
}

function process(url){

    let target_url = getHost() + url;
    return new Promise((resolve)=>{
        request({
            url: encodeURI(target_url),
            headers: headers,
            forever: true,
            json: true
        }, (err, res, body)=>{

            if(err && err.code === 'ECONNREFUSED'){
                return resolve({error: 'ECONNREFUSED'});
            }
        
            if(err){
                console.log(err.stack);
                return resolve(null);
            }
            resolve(body);
        });
    });
}



function sendMoney(postdata){
    let target_url = getHost() + '/transfer';
    return new Promise((resolve) => {
        request.post({url:target_url, form:postdata}, function(error, response, body) {
            if (error || body.toLowerCase().length != 64) {
                console.log(error, body);
                console.log(target_url)
                host_index++;
                return resolve(null);
            }
            resolve(body.toLowerCase());
        })
    });
}
